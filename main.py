import praw
import creds

reddit = praw.Reddit(
    client_id=creds.client_id,
    client_secret=creds.client_secret,
    password=creds.password,
    user_agent="Asher Suicide Prevention (Extension of Quinn) (by u/GaymerWasTaken)",
    username=creds.username
)
subreddits = ["teenagersbutpog"]


def parseContent(content):
    template = []
    if "kill myself" in content or "off myself" in content or "kms" in content or "commit suicide":
        template.append("You are not alone. According to the teen census, over 50 percent of teenagers are suicidal. You may not want help, but I promise you there are people out there that care about you. It may not seem like it, but there are. Death isn't the only way out. I care about you <3")
        template.append("Have you tried talking to people about how you feel? Sometimes just venting helps. How about writing a suicide prevention plan? They are super helpful at walking you through identifying your triggers and ways you'd like to calm down. Check out our suicide prevention plan template at https://go.solusproject.org/suicidepreventionplan")

    if "cut myself" in content or "harm myself" in content or "hurt myself" in content:
        template.append("You're not the only one to self-harm - about 1 in 5 teenagers will self-harm. My advice to you is to first and foremost tell someone you trust. Venting about what's going on can be very helpful. Secondly, try to identify what makes you want to harm yourself. Is it school? Parents? Something else? Identifying what makes you want to hurt yourself will help yourself avoid your triggers. ")
        template.append("The next step is to identify why you self-harm. You likely already know this, and I have some advice for you. If you self-harm to express intense emotions, try painting, sketching, writing, or listening to music. All of these are super helpful! For more information, check the link!")
        template.append("https://go.solusproject.org/stopselfharm")

    if "broke up" in content or "dumped me" in content:
        template.append("Going through a breakup can be a very rough time. I understand that (The human being that made me, that is!). I recommend logging off of Reddit for a little bit and just scrolling through YouTube - preferrably comedy content. Some really good commedians are Drew Lynch and Jeff Dunham. Comedy is a weapon against non-chemical depression.")

    # Return the template
    return template


def parsePosts(title, content):
    template = []
    # Get the title and check it for important information
    template.append(parseContent(title))

    # Get the content and check it for important information
    template.append(parseContent(content))
    return template


for subredditName in subreddits:
    subreddit = reddit.subreddit(subredditName)
    for submission in subreddit.stream.submissions():
        # Reply based on the templates and important information
        submission.reply(' '.join([str(elem) for elem in parsePosts(submission.title.lower(), submission.selftext)]))
